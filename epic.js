document.addEventListener('DOMContentLoaded', function() {
    (
        function() {

        //EpicApplication object
        const EpicApplication = {
            images: [],
            currentType: '',
            dateCache: {},
            imageCache: {}
        };
        
        //Array of image types
        let imgTypes = [
            document.querySelector('select')[0].value,
            document.querySelector('select')[1].value,
            document.querySelector('select')[2].value,
            document.querySelector('select')[3].value
        ];

        //Creates mapping for each imageCache key & creates the keys
        imgTypes.forEach((img) => {
            EpicApplication.imageCache[img] = new Map();
        })
        
        //Displays default page
        function startupPage() {
            const startType = 'natural';
            const imageType = document.querySelector('#type');
            imageType.value = startType;
            getImages(imageType.value);
        }

        document.querySelector('button').addEventListener('click', function(event) {
            event.preventDefault();
            
            let date = document.querySelector('#date').value;
            let imgType = document.querySelector('#type').value;
            let url = `https://epic.gsfc.nasa.gov/api/${imgType}/date/${date}`;

            EpicApplication.currentType = imgType;

            //Checks if the image is already in imageCache if yes, uses the cached data
            if(EpicApplication.imageCache.hasOwnProperty(EpicApplication.currentType) && EpicApplication.imageCache[imgType].has(date)){
                EpicApplication.images = EpicApplication.imageCache[EpicApplication.currentType].get(date);
                const menu = document.querySelector('#image-menu');
                menu.innerHTML = '';

                EpicApplication.images.forEach((image, index) => {
                    const item = document.createElement('li');
                    item.textContent = image.date;
                    item.setAttribute('data-image-list-index', index);
                    menu.appendChild(item);
                });

                document.querySelector('#image-menu').addEventListener('click', function(event) {
                    if (event.target.tagName == 'LI') {
                        const thisDate = event.target.innerText;
                        const [date, time] = thisDate.split(" ");
                        const [year, month, day] = date.split("-");
                        const newDate = `${year}/${month}/${day}`;
                        const index = event.target.getAttribute('data-image-list-index');
                        displayImage(newDate, EpicApplication.currentType, index);
                    }
                });
            }
            else {
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    EpicApplication.images = data;

                    const menu = document.querySelector('#image-menu');
                    menu.innerHTML = '';

                    EpicApplication.images.forEach((image, index) => {
                        const item = document.createElement('li');
                        item.textContent = image.date;
                        item.setAttribute('data-image-list-index', index);
                        menu.appendChild(item);
                    });

  
                    document.querySelector('#image-menu').addEventListener('click', function(event) {
                        if (event.target.tagName == 'LI') {
                            const thisDate = event.target.innerText;
                            const [date, time] = thisDate.split(" ");
                            const [year, month, day] = date.split("-");
                            const newDate = `${year}/${month}/${day}`;
                            const index = event.target.getAttribute('data-image-list-index');
                            
                            displayImage(newDate, EpicApplication.currentType, index);

                            EpicApplication.imageCache[EpicApplication.currentType].set(thisDate.split(" ")[0], EpicApplication.images);

                        }
                    });
                })
                .catch(error => {
                    console.log('Could not fetch the data: ', error);
                });

                const type = document.querySelector('#type');
                type.addEventListener('change', function() {
                    getImages(type.value);
                });

                getImages(EpicApplication.currentType);
            }
        });

        function displayImage(imgDate, imgType, index) {
            const selectedImage = EpicApplication.images[index];
            const url = document.querySelector('#earth-image');
            url.src = `https://epic.gsfc.nasa.gov/archive/${imgType}/${imgDate}/jpg/${selectedImage.image}.jpg`;
            
            const main = document.querySelector('main');
            const date = document.querySelector('#earth-image-date');
            date.textContent = selectedImage.date;

            const title = document.querySelector('#earth-image-title');
            title.textContent = selectedImage.caption;
            
        }

        function getRecentDate(dates) {
            const newDates = dates.sort((a, b) => new Date(b) - new Date(a));
            return newDates[0];
        }

        function getImages(imgType) {
            let url = `https://epic.gsfc.nasa.gov/api/${imgType}/available`;
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    const recentDate = getRecentDate(data);
                    EpicApplication.dateCache[imgType] = recentDate;
                    document.querySelector('#date').value = recentDate;
                })
                .catch(error => {
                    console.log('Could not get the available dates: ', error);
                });
                
        }

        startupPage();

    })();
});
